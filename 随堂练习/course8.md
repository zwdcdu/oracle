# 随堂练习8:RMAN备份与恢复

## 数据库切换到归档日志模式

```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;
```

## 【示例13-5】典型的完整备份和完全恢复案例

```sql
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;

输出4个备份集列表
===================


BS 关键字  类型 LV 大小       设备类型 经过时间 完成时间       
------- ---- -- ---------- ----------- ------------ -------------------
1       Full    1.01G      DISK        00:00:06     2023-05-15 06:10:42
        BP 关键字: 1   状态: AVAILABLE  已压缩: NO  标记: TAG20230515T061036
段名:/home/oracle/app/oracle/recovery_area/ORCL/backupset/2023_05_15/o1_mf_nnndf_TAG20230515T061036_l62q2wv8_.bkp
  备份集 1 中的数据文件列表
  File LV Type Ckp SCN    Ckp 时间          Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- ------------------- ----------- ------ ----
  1       Full 2819771    2023-05-15 06:10:36              NO    /home/oracle/app/oracle/oradata/orcl/system01.dbf
  3       Full 2819771    2023-05-15 06:10:36              NO    /home/oracle/app/oracle/oradata/orcl/sysaux01.dbf
  4       Full 2819771    2023-05-15 06:10:36              NO    /home/oracle/app/oracle/oradata/orcl/undotbs01.dbf
  7       Full 2819771    2023-05-15 06:10:36              NO    /home/oracle/app/oracle/oradata/orcl/users01.dbf

BS 关键字  类型 LV 大小       设备类型 经过时间 完成时间       
------- ---- -- ---------- ----------- ------------ -------------------
2       Full    496.88M    DISK        00:00:02     2023-05-15 06:10:45
        BP 关键字: 2   状态: AVAILABLE  已压缩: NO  标记: TAG20230515T061036
段名:/home/oracle/app/oracle/recovery_area/ORCL/F5B27895F4803A83E05385D6A8C089F0/backupset/2023_05_15/o1_mf_nnndf_TAG20230515T061036_l62q33yx_.bkp
  备份集 2 中的数据文件列表
  容器 ID: 3, PDB 名称: PDBORCL
  File LV Type Ckp SCN    Ckp 时间          Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- ------------------- ----------- ------ ----
  9       Full 2819775    2023-05-15 06:10:43              NO    /home/oracle/app/oracle/oradata/orcl/pdborcl/system01.dbf
  10      Full 2819775    2023-05-15 06:10:43              NO    /home/oracle/app/oracle/oradata/orcl/pdborcl/sysaux01.dbf
  11      Full 2819775    2023-05-15 06:10:43              NO    /home/oracle/app/oracle/oradata/orcl/pdborcl/undotbs01.dbf
  12      Full 2819775    2023-05-15 06:10:43              NO    /home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf

BS 关键字  类型 LV 大小       设备类型 经过时间 完成时间       
------- ---- -- ---------- ----------- ------------ -------------------
3       Full    512.77M    DISK        00:00:01     2023-05-15 06:10:48
        BP 关键字: 3   状态: AVAILABLE  已压缩: NO  标记: TAG20230515T061036
段名:/home/oracle/app/oracle/recovery_area/ORCL/F5B26AE608083293E05385D6A8C0E5C1/backupset/2023_05_15/o1_mf_nnndf_TAG20230515T061036_l62q3721_.bkp
  备份集 3 中的数据文件列表
  容器 ID: 2, PDB 名称: PDB$SEED
  File LV Type Ckp SCN    Ckp 时间          Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- ------------------- ----------- ------ ----
  5       Full 1441668    2023-02-28 01:03:49              NO    /home/oracle/app/oracle/oradata/orcl/pdbseed/system01.dbf
  6       Full 1441668    2023-02-28 01:03:49              NO    /home/oracle/app/oracle/oradata/orcl/pdbseed/sysaux01.dbf
  8       Full 1441668    2023-02-28 01:03:49              NO    /home/oracle/app/oracle/oradata/orcl/pdbseed/undotbs01.dbf

BS 关键字  类型 LV 大小       设备类型 经过时间 完成时间       
------- ---- -- ---------- ----------- ------------ -------------------
4       Full    17.94M     DISK        00:00:00     2023-05-15 06:10:50
        BP 关键字: 4   状态: AVAILABLE  已压缩: NO  标记: TAG20230515T061050
段名:/home/oracle/app/oracle/recovery_area/ORCL/autobackup/2023_05_15/o1_mf_s_1136873450_l62q3b8g_.bkp
  包含的 SPFILE: 修改时间: 2023-05-15 06:05:20
  SPFILE db_unique_name: ORCL
  包括的控制文件: Ckp SCN: 2819783      Ckp 时间: 2023-05-15 06:10:50

备份完成后，登录hr用户，修改一些表的数据！

删除数据文件:

RMAN> host "rm /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "rm /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";

开始恢复:
RMAN> SHUTDOWN IMMEDIATE;
RMAN> SHUTDOWN ABORT;
RMAN> STARTUP MOUNT;
RMAN> RESTORE DATABASE;
RMAN> recover database;
RMAN> ALTER DATABASE OPEN;
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";
/home/oracle/app/oracle/oradata/orcl/pdborcl/sysaux01.dbf  /home/oracle/app/oracle/oradata/orcl/pdborcl/undotbs01.dbf
/home/oracle/app/oracle/oradata/orcl/pdborcl/system01.dbf  /home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
/home/oracle/app/oracle/oradata/orcl/pdborcl/temp01.dbf
主机命令完成

恢复完成，登录hr用户，看看数据是否完全恢复！

最后删除备份：
RMAN> delete backup;
RMAN> list backup;
说明与资料档案库中的任何备份都不匹配
```

## 不完全恢复（恢复到时间点）案例

```sql
RMAN>
backup as compressed backupset database;
list backup；
delete obsolete；
report obsolete；

$ sqlplus hr/123@pdborcl;
SQL> set time on
03：15：04 SQL> SELECT job_id,min_salary FROM jobs WHERE  job_id='PR_REP';
第1次修改：
03：15：13 SQL> UPDATE jobs SET min_salary=100 WHERE  job_id='PR_REP';
03：16：51 SQL> commit;
第2次修改，操作员错误地把所有min_salary都设置为0：
03：17：00 SQL> UPDATE jobs SET min_salary=0;
03：17：04 SQL> commit;
03：17：08 SQL> SELECT job_id,min_salary FROM jobs;
03：19：00 SQL> exit

仅关闭PDBORCL：
$ sqlplus / as sysdba
SQL> ALTER SESSION SET CONTAINER =pdborcl;
SQL> shutdown immediate;
SQL> exit

关闭PDBORCL之后,通过RMAN中做数据库不完全恢复(下面的语句的时间应是第1次修改commit时间加1秒)：
$ rman target /
RMAN> restore pluggable database pdborcl until time "to_date('2023-05-15 11:50:40','yyyy-mm-dd hh24:mi:ss')";
RMAN> recover pluggable database pdborcl until time "to_date('2023-05-15 11:50:40','yyyy-mm-dd hh24:mi:ss')";
RMAN> ALTER pluggable database pdborcl open resetlogs;

打开数据库成功之后,看看数据是否恢复到了第1次修改后（第2次修改前）的时刻：
$ sqlplus hr/***@pdborcl;
SQL> SELECT job_id,min_salary FROM jobs WHERE  job_id='PR_REP';
结果为第1次修改后的值100
第2次修改后的数据会丢失。

```
