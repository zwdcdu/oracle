# 随堂练习5:定时作业

## 给HR用户授权，让该用户可以创建作业任务

```sql
$ sqlplus system/123@pdborcl
SQL>GRANT create job TO hr;
SQL>GRANT manage scheduler TO hr;
```

## 创建作业并立即执行

- 'FREQ=SECONDLY;INTERVAL=2' 表示每2秒执行一次作业
- start_date => NULL 表示立即执行

```sql
$ sqlplus hr/123@pdborcl
SQL>
BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
     job_name => '"HR"."JOB_CALC"',
     job_type => 'PLSQL_BLOCK',
     job_action => 'begin update hr.employees set salary=salary+1;commit;end;',
     number_of_arguments => 0,
     start_date => NULL,
     repeat_interval => 'FREQ=SECONDLY;INTERVAL=2',
      end_date => NULL,
      enabled => TRUE,
      auto_drop => FALSE,
      comments => '');
END;
/
```

## 查询用户hr的Job

```sql
$ sqlplus hr/123@pdborcl
COL job_name FORMAT a15
COL start_date FORMAT a30
COL next_run_date FORMAT a30
SELECT job_name,to_char(START_DATE,'yyyy-mm-dd hh24:mi:ss')START_DATE,to_char(NEXT_RUN_DATE,'yyyy-mm-dd hh24:mi:ss')NEXT_RUN_DATE FROM user_scheduler_jobs;
```

## 查询salary的变化

```sql
select salary from hr.employees;
select sum(salary) from hr.employees;
```

## 删除作业

```sql
BEGIN
DBMS_SCHEDULER.DROP_JOB(job_name => '"HR"."JOB_CALC"',
defer => false,force => false);
END;
/
```
