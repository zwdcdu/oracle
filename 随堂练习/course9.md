# 随堂练习9:数据库的导出/导入

## 创建目录expdir

```sh
$ cd ~
$ mkdir expdir
$ cd expdir
$ pwd
$ sqlplus system/123@pdborcl
SQL> create or replace directory expdir as '/home/oracle/expdir';
SQL> GRANT read,write on directory expdir to hr;
Grant succeeded.
```

## expdp导出

本示例导出hr用户的所有对象,导出文件名是hr.dmp,目录是expdir。

```sh
$expdp hr/123@pdborcl directory=expdir dumpfile=hr.dmp
$ls ~/expdir
export.log  hr.dmp
```

## impdp导入

- expdp和impdp的一个重要用途之一就是做数据迁移,比如将数据迁移到其他服务器或者用户之中。hr.dmp中的对象是由hr用户创建的,本示例就是将hr.dmp导入到hr2新用户中,导入成功之后,所有对象的用户会由hr变为hr2。下面首先创建用户hr2:

```sql
$ sqlplus system/123@pdborcl
CREATE USER hr2 IDENTIFIED BY 123 default tablespace "USERS" temporary tablespace "TEMP";
ALTER USER hr2 quota unlimited on USERS;
ALTER USER hr2 quota unlimited on SYSAUX;
GRANT "CONNECT" to hr2 with admin option;
GRANT "RESOURCE" to hr2 with admin option;
ALTER USER hr2 default role "CONNECT","RESOURCE";
GRANT CREATE JOB TO hr2 with admin option;
GRANT CREATE VIEW TO hr2 with admin option;
GRANT READ,WRITE ON DIRECTORY expdir TO hr2;
```

- 用户hr2创建成功后,可以由SYSTEM用户来做导入操作(hr用户的权限不够),导入命令中使用参数“remap_schema=hr:hr2”将hr用户对象改变为hr2的对象,使用参数“transform=disable_archive_logging:y”在导入时禁用归档日志,提高导入的速度。

```sh
$impdp system/123@pdborcl directory=expdir dumpfile=hr.dmp remap_schema=hr:hr2 transform=disable_archive_logging:y

登录hr2，查看数据导入是否成功：
$ sqlplus hr2/123@pdborcl
SQL> select * from jobs;
已选择 19 行。
```

## 实验完成，删除hr2用户

```sql
$ sqlplus system/123@pdborcl
SQL>drop user hr2 cascade;
用户已删除。
```
