# Oracle 数据库应用

- ![book](./book.png)

## 课程资源

- gitlab账号录入地址：http://39.100.109.37:1433/
- [老师的资源](https://gitlab.com/zwdcdu/oracle)
- [openjdk-11+28_linux-x64_bin.tar.gz下载](https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_linux-x64_bin.tar.gz)

## 内容说明

- PPT目录：各章节的PPT
- script目录：各章节的SQL语句及源码
- doc目录：参考文档
- book.pdf： 本书的PDF文档

## 学习工具

- git协议：用于提交实验报告
- git账号：每个同学必须有一个gitlab账号，并上传SSH公钥
- VSCode: 实验报告编写
- 老师的Centos7系统桌面4个sqlplus图标，修改为：sh -c "rlwrap 原内容"

## 实验准备

- 每个同学必须有一个gitlab账号，并上传SSH公钥。
- 在你的账号下创建一个**公共(public)的项目oracle**，全是小写字母。
- 当你创建好账号和公共项目后，老师通过 https://gitlab.com/你的账号/oracle 这个地址就可以批阅你的实验报告了。
- 正式提交的实验一共5次,期末考查一次，每个同学各自提交，不组成团队。
- 在oracle项目的主目录中创建5个文件夹：test1,test2,test3,test4,test5,test6，全是小写字母。test1至test5表示5次实验，test6表示期末项目测验。每次实验的源代码以及说明文件放在各自的文件夹中。
- 实验报告用markdown格式文件编写。每个目录中都必须有一个主文件README.md。注意README是大写字母，md是小写字母。
- 每个目录中必须有一个文件README.md。注意大写小写。在文档开头写上实验人的班级，学号，姓名，后面才写实验名称，实验目的，实验步骤等实验内容。
- 最好用vscode编辑工具编写实验报告，用git方式提交，需要在windows系统中安装git环境工具，如果没有git环境，可以从这里下载[gitgfb_ttrar.rar](./gitgfb_ttrar.rar)。
- 本期所有实验都完成后，你的oracle项目下面应该是这样的

```text
oracle
   test1
      README.md
      pict1.png
      pict2.png
   test2
      README.md
      pict1.png
      pict2.png
   test3
      README.md
      pict1.png
      pict2.png
   test4
      README.md
      pict1.png
      pict2.png
   test5
      README.md
      pict1.png
      pict2.png           
   test6
      README.md
      pict1.png
      pict2.png
      期末考核报告.docx
      期末考核报告.pdf
```

## 期末考查

- [期末考查说明](./%E4%B8%8A%E4%BA%A4%E5%AE%9E%E9%AA%8C/test6.md)
