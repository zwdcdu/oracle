# 随堂练习6:表空间管理

## 增加表空间users02

- 初始大小为10M
- 不允许自动增加

```sql
$sqlplus system/123@pdborcl

SQL>CREATE TABLESPACE users02 DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf'
  SIZE 10M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

--使用linux命令ls查看数据文件
SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf -lh

```

## 查询表空间users02

```sql
$ sqlplus system/123@pdborcl
SQL>
SELECT TABLESPACE_NAME,STATUS,CONTENTS,LOGGING FROM dba_tablespaces;

SELECT a.tablespace_name "表空间名",Total "大小",
 free "剩余",( total - free )"使用",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

## 创建表test1存储在表空间users02中

- 下面的脚本可通过sqldeveloper的表的“快速DDL”菜单生成。

```sql
$sqlplus hr/123@pdborcl
SQL>
 CREATE TABLE "HR"."TEST1" 
   ("ID" NUMBER(*,0), 
	"NAME" VARCHAR2(200 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS02" ;

  ALTER TABLE "HR"."TEST1" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."TEST1" MODIFY ("NAME" NOT NULL ENABLE);
```

## 在test1表中插入数据，直到表空间不足

```sql
$sqlplus hr/123@pdborcl
SQL>
insert into test1 (id,name) values(1,'abcdefg');
commit;
--重复运行17次左右以下命令，test1自我复制，直到表空间不足
insert into test1 select id,name from test1;
commit;
```

- 表空间不足时显示：

```text
在行: 1 上开始执行命令时出错 -
insert into test1 select id,name from test1
错误报告 -
ORA-01653: 表 HR.TEST1 无法通过 128 (在表空间 USERS02 中) 扩展
```

## 扩展表空间并查询表空间使用情况

```sql
$ sqlplus system/123@pdborcl
SQL>
ALTER database datafile '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf' RESIZE 120M;
```

## 再次插入数据并再次查询表空间使用情况

- 由于空间扩展为120M，再次插入数据应该插入成功。当然多插入几次以后，空间仍然不足。

## 实验完成删除表空间

- 下面的语句会删除表空间的文件以及所有表！

```sql
$ sqlplus system/123@pdborcl
SQL>
DROP TABLESPACE users02 INCLUDING CONTENTS AND DATAFILES;
```
