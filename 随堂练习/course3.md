# 随堂练习3:创建pdborcl的克隆数据库clonedb

## 创建

本示例将pdborcl克隆成了clonedb。注意,在克隆之前,需要首先关闭pdborcl,然后打开为只读状态,这样避免了在克隆过程中pdborcl有数据修改。克隆完成后,需要打开pdborcl和clonedb为正常的读写模式。

```text
$ sqlplus / as sysdba
SQL>ALTER PLUGGABLE DATABASE pdborcl CLOSE;
SQL>ALTER PLUGGABLE DATABASE pdborcl OPEN READ ONLY;
SQL>CREATE PLUGGABLE DATABASE clonedb FROM pdborcl
 file_name_convert=
 ('/home/oracle/app/oracle/oradata/orcl/pdborcl',
 '/home/oracle/app/oracle/oradata/orcl/clonedb/');
SQL>ALTER PLUGGABLE DATABASE pdborcl CLOSE;
SQL>ALTER PLUGGABLE DATABASE pdborcl OPEN;
SQL>ALTER PLUGGABLE DATABASE clonedb OPEN;
SQL>show pdbs;
查看有没有clonedb，状态是不是READ WRITE

```

## 检测

用clonedb中的hr用户登录clonedb

```sh
$ ls -l /home/oracle/app/oracle/oradata/orcl/clonedb/
查看数据库文件所在目录

$lsnrctl status
查看有没有clonedb的监听。

$sqlplus hr/123@localhost/clonedb
SQL>select * from jobs;

```

## 设置clonedb的tnsnames.ora

- 在 $ORACLE_HOME/network/admin/tnsnames.ora文件中添加

```text
CLONEDB_S =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVER = SHARED)
      (SERVICE_NAME = clonedb)
    )
  )

CLONEDB_D =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = clonedb)
    )
  )
```

- 登录

```sh
$sqlplus hr/123@clonedb_s
SQL>select * from jobs;

$sqlplus hr/123@clonedb_d
SQL>select * from jobs;
```
