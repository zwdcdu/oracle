# 随堂练习7:归档日志模式

## 数据库切换到归档日志模式

```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;
```

## 查询在线重做日志redo log

```sql
$ sqlplus / as sysdba
SQL> col member format a50
SQL> SELECT group#,member FROM v$logfile;
SQL> SELECT group#,archived,status FROM v$log;
  GROUP#   ARC  STATUS
---------- --- ----------------
	 1       NO   CURRENT
	 2       YES  INACTIVE
	 3       YES  INACTIVE

```

## 查询当前归档模式archive log

```sql
SQL> archive log list
SQL> show parameter db_recover
SQL> col name format a50
SQL> SELECT NAME,SPACE_LIMIT,SPACE_USED FROM v$recovery_file_dest;
NAME						                      SPACE_LIMIT SPACE_USED
------------------------------------------- ----------- ----------
/home/oracle/app/oracle/recovery_area		    1.3401E+10		0

输出的目录/home/oracle/app/oracle/recovery_area/是归档日志的总目录。
```

## 查询归档文件

```shell
$ ls /home/oracle/app/oracle/recovery_area/ORCL/archivelog
2023_03_21	2023_03_27  2023_03_28	2023_04_03  2023_04_04	2023_04_06  2023_04_10
```

## 创建测试表test1

```sql
$sqlplus hr/123@pdborcl
SQL>
 CREATE TABLE "HR"."TEST1" 
   ("ID" NUMBER(*,0), 
	"NAME" VARCHAR2(200 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT);

  ALTER TABLE "HR"."TEST1" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."TEST1" MODIFY ("NAME" NOT NULL ENABLE);
```

## 在test1表中insert/update/delete数据

```sql
$sqlplus hr/123@pdborcl
SQL>
insert into test1 (id,name) values(1,'abcdefg');
commit;
--test1自我复制，直到表空间不足
insert into test1 select id,name from test1;
commit;

update test1 set name='abcde';
commit;

delete from test1;
commit;
```

- 也可以将上面的commit改为rollback。回滚到上一次的commit。

## 分步insert/update/delete表test1，观察重做日志和归档日志文件的变化

- 观察语句SELECT group#,archived,status FROM v$log;的结果列ARC/STATUS的变化。
  - ARC由NO变为YES表示归档。这里可以看归档文件
  - STATUS由INACTIVE变为current表示切换了在线重做日志组。
- 观察目录中/home/oracle/app/oracle/recovery_area/ORCL/archivelog当天目录的文件大小的变化。
  - ls /home/oracle/app/oracle/recovery_area/ORCL/archivelog/2023_04_11
  - 随着操作insert/update/delete的增多，目录中的归档日志文件*.arc会越来越多。

```shell
$ cd /home/oracle/app/oracle/recovery_area/ORCL/archivelog/2023_04_11
$ sqlplus / as sysdba
SQL>SELECT group#,archived,status FROM v$log;
SQL>!ls -lh

能看到一些*.arc文件表示实验成功。
```

## 实验结束，切换回非归档模式

```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE NOARCHIVELOG;
SQL> ALTER DATABASE OPEN;
SQL> archive log list
```

## 删除所有归档日志文件*.arc

```shell
$rm /home/oracle/app/oracle/recovery_area/ORCL/archivelog/2023_04_11/*.arc
```

